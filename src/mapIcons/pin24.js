import pin24 from 'file-loader!../assets/mapPins/pin24.svg'

// Создаёт iconLayout для балуна
export default function (ymaps) {
  return ymaps.templateLayoutFactory.createClass(pin24)
}
