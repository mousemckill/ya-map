export default [{
  coordinates: [55.8, 37.6],
  balloon: {
    balloonContentHeader: 'Метка № 1',
    balloonContentBody: 'Не мысля гордый свет забавить,</br>' +
      'Вниманье дружбы возлюбя,</br>' +
      'Хотел бы я тебе представить</br>' +
      'Залог достойнее тебя,'
  },
  icon: {
    iconLayout: 'default#image',
    iconImageHref: require('./assets/mapPins/pin24.png')
  }
}]
